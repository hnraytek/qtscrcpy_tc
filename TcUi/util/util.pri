include ($$PWD/mousetap/mousetap.pri)

HEADERS += \
    $$PWD/compat.h \
    $$PWD/bufferutil.h \
    $$PWD/config.h \
    $$PWD/logout.h

SOURCES += \
    $$PWD/bufferutil.cpp \
    $$PWD/config.cpp \
    $$PWD/logout.cpp
